create table if not exists account (
  id bigint auto_increment PRIMARY KEY NOT NULL ,
  amount bigint NOT NULL,
  version number NOT NULL
);

create table if not exists money_transfer_log (
  date DATETIME,
  accountFrom VARCHAR(36),
  accountTo VARCHAR(36),
  amount number
  --TODO: foreign keys
);

-- INSERT into account(id, amount) values ('1', 1000000000);
-- INSERT into account(id, amount) values ('2', 1000000000);
