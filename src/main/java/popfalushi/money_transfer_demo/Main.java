package popfalushi.money_transfer_demo;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.oracle.webservices.internal.api.message.ContentType;
import org.eclipse.jetty.http.HttpHeader;
import org.eclipse.jetty.http.MimeTypes;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import static spark.Spark.*;
/**
 * Created by 1 on 29.05.2016.
 */
public class Main {
    private static Logger log = LoggerFactory.getLogger(Dao.class);

    public static void main(String[] args) {
        Dao dao = new Dao();
        int maxThreads = 8;
        int minThreads = 2;
        int timeOutMillis = 30000;
        threadPool(maxThreads, minThreads, timeOutMillis);
        get("/accounts/sum", (request, response) -> {
            response.header(HttpHeader.CONTENT_TYPE.toString(), MimeTypes.Type.APPLICATION_JSON.toString());
            Long sum = dao.getAccountsSum();
            log.debug("Sum of all accounts: {}", sum);
            return "{\"sum\": " + sum + "}";
        });
        get("/accounts/:id", (request, response) -> {
            response.header(HttpHeader.CONTENT_TYPE.toString(), MimeTypes.Type.APPLICATION_JSON.toString());
            String idStr = request.params("id");
            Long id = Long.valueOf(idStr);
            Account account = dao.getAccount(id);
            Gson gson = new Gson();
            log.debug("return id {}", idStr);
            return gson.toJson(account);
        });

        post("/accounts", (request, response) -> {
            log.debug("1 creating account");
            log.debug(request.queryParams().toString());
            String amountStr = request.queryParams("amount");
            Long amount = Long.valueOf(amountStr);
            log.debug("creating account");
            Long id = dao.createAccount(amount);
            log.debug("finished creating account {}", id);
            return id;
        });

        post("/accounts/:id/deposit", (request, response) -> {
            String idStr = request.params("id");
            Long id = Long.valueOf(idStr);
            String amountStr = request.queryParams("amount");
            Long amount = Long.valueOf(amountStr);
            dao.deposit(id, amount);
            return "fff";
        });

        post("/accounts/:id/withdraw", (request, response) -> {
            String idStr = request.params("id");
            Long id = Long.valueOf(idStr);
            String amountStr = request.queryParams("amount");
            Long amount = Long.valueOf(amountStr);
            dao.withdraw(id, amount);
            return null;
        });
        post("/accounts/:id/money-transfer", (request, response) -> {
            log.debug("Start money transfer");
            String idStr = request.params("id");
            Long id = Long.valueOf(idStr);
            String toStr = request.queryParams("to");
            Long to = Long.valueOf(toStr);
            String amountStr = request.queryParams("amount");
            Long amount = Long.valueOf(amountStr);
            log.debug("Start money transfer from {} to {}", id, to);
            dao.moneyTransfer(id, to, amount);
            log.debug("Successful money transfer from {} to {}", id, to);
            return "done";
        });

        exception(Exception.class, (exception, request, response) -> {
            log.error("Exception:", exception);
        });
    }
}
