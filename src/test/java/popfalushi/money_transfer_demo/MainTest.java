package popfalushi.money_transfer_demo;

import org.junit.AfterClass;
import org.junit.BeforeClass;
import org.junit.Test;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.time.Duration;
import java.time.Instant;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.concurrent.Callable;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;

import static com.jayway.restassured.RestAssured.get;
import static com.jayway.restassured.RestAssured.given;
import static org.hamcrest.CoreMatchers.equalTo;
import static spark.Spark.stop;

/**
 * Created by 1 on 29.05.2016.
 */
public class MainTest {
    private static Logger log = LoggerFactory.getLogger(MainTest.class);

    private static int NUMBER_OF_ACCOUNTS = 200;
    private static int NUMBER_OF_TRANSFERS = 10000;
    private static Integer STARTING_SUM = 500000;
    @BeforeClass
    public static void init() throws Exception{
        Main.main(null);
    }
    @Test
    public void test() throws Exception{
        get("http://localhost:4567/accounts/sum").then().assertThat().body("sum", equalTo(0));
        for(int i = 0; i < NUMBER_OF_ACCOUNTS; i++){
            given().
                    formParam("amount", STARTING_SUM).
                    post("http://localhost:4567/accounts").
                    then().
                    body(equalTo(Integer.toString(i+1)));
        }
        get("http://localhost:4567/accounts/1").then()
                .assertThat().body("id", equalTo(1))
                .assertThat().body("amount", equalTo(STARTING_SUM))
                .assertThat().body("version", equalTo(0));
        get("http://localhost:4567/accounts/sum").then().assertThat().body("sum", equalTo(STARTING_SUM*NUMBER_OF_ACCOUNTS));

        ExecutorService ex = Executors.newFixedThreadPool(1);
        List<Callable<Integer>> tasks = new ArrayList<>();
        for(int i = 0; i < NUMBER_OF_TRANSFERS; i++){
            Integer from = (i%NUMBER_OF_ACCOUNTS) + 1;
            Integer to = NUMBER_OF_ACCOUNTS + 1 - from;
            tasks.add(()->{
                given().
                        formParam("to", to).
                        pathParam("from", from).
                        formParam("amount", "1").
                        post("http://localhost:4567/accounts/{from}/money-transfer").
                        then().
                        body(equalTo("done"));
                return 1;
            });
        }
        Collections.shuffle(tasks);
        Instant start = Instant.now();
        List<Future<Integer>> futures = ex.invokeAll(tasks);
        Instant end = Instant.now();
        log.info("Duration: {}", Duration.between(end, start).toString());
        get("http://localhost:4567/accounts/sum").then().assertThat().body("sum", equalTo(STARTING_SUM*NUMBER_OF_ACCOUNTS));
    }

    @AfterClass
    public static void after(){
        stop();
    }
}
